<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        "number",
        "invoice_date",
        "supply_date",
        "comment",
    ];
}
