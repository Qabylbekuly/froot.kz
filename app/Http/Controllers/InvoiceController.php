<?php

namespace App\Http\Controllers;

use App\Models\FormRequest;
use App\Models\Invoice;
use App\ToursTicket;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function invoiceGetView(Request $request, $id)
    {
        return view('admin.invoice.detail', compact('id'));
    }

    public function invoiceListView()
    {
        $role = Auth::user()->roles[0]['slug'];
        return view('admin.invoice.index', compact('role'));
    }

    public function invoiceGet(Request $request, $id)
    {
        $invoice = Invoice::where('id', $id)->first();
        return response()->json([
            'result' => 1,
            'status' => 200,
            'invoice' => $invoice,
        ], JsonResponse::HTTP_OK);
    }

    public function invoiceDelete(Request $request, $id)
    {
        $invoice = Invoice::find($id)->delete();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'result' => 1,
            'status' => 200,
        ]);

    }

    public function invoicePost(Request $request, $id)
    {
        $validatedData = $request->validate([
            'invoice.number' => 'required',
            'invoice.invoice_date' => 'required',
            'invoice.supply_date' => 'required',
            'invoice.comment' => '',
        ]);
        $request = $request->all();
        if (isset($request['invoice']['id']) && $request['invoice']['id'] == 0) {
            $invoice = new Invoice();
        } else {
            $invoice = Invoice::where('id', $request['invoice']['id'])->first();
        }
        $invoice->fill($request['invoice']);
        if ($invoice->save()) {
            return response()->json([
                'result' => 1,
                'status' => 200,
                'invoice' => $invoice,
            ], JsonResponse::HTTP_OK);
        } else {
            return response()->json([
                'result' => 0,
                'status' => 200,
            ], JsonResponse::HTTP_CONFLICT);
        }

    }

    public function invoiceList()
    {
        $invoice = Invoice::all();
        return response()->json([
            'result' => 1,
            'status' => 200,
            'invoices' => $invoice,
        ], JsonResponse::HTTP_OK);
    }


}
