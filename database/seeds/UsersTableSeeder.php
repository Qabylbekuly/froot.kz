<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '1',
            'name' => 'admin',
            'email' => 'admin@froot.kz',
            'password' => bcrypt('123456'),
        ]);
        DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1',
        ]);

        DB::table('users')->insert([
            'id' => '2',
            'name' => 'Moderator',
            'email' => 'moderator@froot.kz',
            'password' => bcrypt('123456'),
        ]);
        DB::table('role_user')->insert([
            'role_id' => '4',
            'user_id' => '2',
        ]);


    }
}
