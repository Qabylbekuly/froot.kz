<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin/invoices/{id}', 'InvoiceController@invoiceGetView')->name('home');
Route::get('/admin/invoices', 'InvoiceController@invoiceListView')->name('home');

Route::get('/api/admin/invoices/{id}', 'InvoiceController@invoiceGet')->name('home');
Route::post('/api/admin/invoices/{id}', 'InvoiceController@invoicePost')->name('home');
Route::post('/api/admin/invoices/delete/{id}', 'InvoiceController@invoiceDelete')->name('home');
Route::get('/api/admin/invoices', 'InvoiceController@invoiceList')->name('home');

