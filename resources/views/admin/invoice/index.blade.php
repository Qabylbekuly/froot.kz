@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-info">
                    <h1>Invoices
                        <hr>
                    </h1>
                </div>

                <div class="card">
                    <div class="card-header">Actions</div>

                    <div class="card-body">
                        <a href="/admin/invoices/0" class="btn btn-outline-primary">Add new</a>

                    </div>
                </div>

                <div class="card mt-4">
                    <div class="card-header">Invoices</div>

                    <div class="card-body">

                        <admin-invoice-list :role="'{{$role}}'" :auth="'{{\Illuminate\Support\Facades\Auth::user()}}'"></admin-invoice-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
