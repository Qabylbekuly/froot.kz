@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-info">
                    <div class="row d-flex align-items-center">
                        <div class="col-md-6"><h1>Create Invoice</h1></div>
                        <div class="col-md-6 text-right"><a href="/admin/invoices/" class="btn btn-success">All
                                Invoice</a></div>
                        <hr>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <admin-invoice-detail :id="{{$id}}"></admin-invoice-detail>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
