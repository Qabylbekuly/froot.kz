require('./bootstrap');
window.Vue = require('vue');
Vue.component('admin-invoice-list', require('./components/admin/invoice/list.vue').default);
Vue.component('admin-invoice-detail', require('./components/admin/invoice/detail.vue').default);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


const app = new Vue({
    el: '#app',
});
